<?php
  require_once '../dbconfig.php';
  require_once '../key.php';
  $title="Library";
  
  $sql="SELECT book.title, book.summary, category.name, book.ebook, book.picture 
	  FROM book INNER JOIN category
	  ON book.category = category.id
	  WHERE confirmtime !='0000-00-00 00:00:00'";
	  $result = mysqli_query($dbConnect,  $sql);
	  if (!$result)
	  {
	  	$_SESSION['error']="Material selection failed: ". mysqli_error($dbConnect);
        header("Location: $url"."error/");
		exit();
	  }
	  //$row = mysqli_fetch_array($result);
      //$right=$row['activate'];
	  
	  //Construct HTML for display
	  $tableHead="<table border='1' width='700'>
	    <tr>
	      <td>Title</td> <td>Content</td> <td>Front Cover</td> 
	      <td>Category</td> <td>Download</td>   
	    </tr>";
	  $tableMiddle="";
	   while ($row = mysqli_fetch_array($result))
	   {
	   	$tableMiddle=$tableMiddle."<tr>
	   	<td>".$row['title']."</td>
	   	<td>".$row['summary']."</td>
	   	<td><img src='$url"."bookpictures/".$row['picture']."' height='120' width='120'/>
	   	<td>".$row['name']."</td>
	   	<td><a href='$url"."booksoftcopy/".$row['ebook']."'>Download</a></td>
		</tr>";
	   }
	   $tableTail="</table> ";
	   $output=$tableHead.$tableMiddle.$tableTail;
  
  require_once '../templates/header.php';