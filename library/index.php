<?php
  require_once 'controller.php';
  $pageName="<span id='pageName'>Library</span>";
?>
	<body>
		<?php require_once '../templates/whitebarleft.php'; ?>
			<div class="left" id="wrapperRight">
    			<?php echo $output; ?>
        	</div><!--right wrapper-->
		</div><!--wrapper-->
		<?php require_once '../templates/footer.php'; ?>
	</body>
</html>
