<?php
  require_once 'controller.php';
  $pageName="<span id='pageName'>Registeration</span>";
?>
	<body>
		<?php require_once '../templates/whitebarleft.php'; ?>
			<div class="left" id="wrapperRight">
				<?php if(isset($successMessage)) echo $successMessage; ?>
				<?php if(isset($errorMessage)) echo $errorMessage; ?>
				<form action="processor.php" method="post" enctype="multipart/form-data">
			<table id="regTable">
				<tr <?php if(isset($nameErrorBg)) echo $nameErrorBg; ?> >
					<td>Name</td>
					<td><input  type="text" name="name" <?php if(isset($nameValue)) echo $nameValue; ?> /></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><input type="text" name="email" /></td>
				</tr>
				<tr>
					<td>Phone</td>
					<td><input  type="text" name="phone" /></td>
				</tr>
				<tr>
					<td>Password</td>
					<td><input type="password" name="password" /></td>
				</tr>
				<tr>
					<td>
					  Upload Passport
					  <span class='small_note'>.jpg, .gif or .png max 0.1MB</span>
          			  <span class='small_note'>Ideal Size 150px by 150px</span>
				    </td>
					<input type='hidden' name='MAX_FILE_SIZE' value='150000'/>
					<td><input type="file" name="passport" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input class="submit" type="submit" name="register" value="REGISTER"/></td>
				</tr>
			</table>
		</form>
	</div><!--right wrapper-->
		</div><!--wrapper-->
		<?php require_once '../templates/footer.php'; ?>
	</body>
</html>
