<?php
  require_once '../dbconfig.php';
  
  //Registration Successful
  if(isset($_SESSION['regSuccessful'])){
  	$successMessage="Registeration Successful Please <a href='$url'>Login</a>";
	unset($_SESSION['regSuccessful']);
  }
  
  //Error Checking
  if(isset($_SESSION['dataError'])){
  	$errorMessage="<span class='error'>Form Not Properly Filled</span>";
	unset($_SESSION['dataError']);
	
	//Check for name error
	if(isset($_SESSION['nameError'])){
	  $nameErrorBg="class='errorBg'";
	  unset($_SESSION['nameError']);
	}
  
	//Prepopulation of data
	$nameValue="value='".$_SESSION['nameValue']."'";
	$emailValue="value='".$_SESSION['emailValue']."'";
	$phoneValue="value='".$_SESSION['phoneValue']."'";
	unset($_SESSION['nameValue'], $_SESSION['emailValue'], $_SESSION['phoneValue']);
  }
  
  $title="Registration";
  
  
  
  require_once '../templates/header.php';