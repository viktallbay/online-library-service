<?php
  session_start();
  $_SESSION = array();
  session_destroy();
  require_once '../config.php';
  $title="Logout";
  require_once '../templates/header.php';