<?php 
/*This function sanitizerString help prevent malicious input been enter into your program
via a form on your site
use it like this "$variable = sanitizeString($_POST['user_input']);"*/
function sanitizeString($var)
{
	$var = stripslashes($var);
	$var = htmlspecialchars($var,ENT_QUOTES, 'UTF-8');
	$var = strip_tags($var);
	return $var;
}

/*This function sanitizeMySQL prevents malicious input been enter into your database
via a form on your site
use it like this "$variable = sanitizeMySQL($_POST['user_input']);"*/
function sanitizeMySQL($var1, $var)
{
	$var = mysqli_real_escape_string($var1, $var);
	$var = sanitizeString($var);
	return $var;
}
//check that gender supplied to the script is either male or female
/*function checkGender($var)
{
  if($var=='male')
  {
  	return TRUE;
  }
  elseif ($var=='female') {
    return TRUE;  
  }
  else {
    return FALSE;
  }
}*/
// Generate a human readable right on this system
function getRight($var)
{
	switch ($var)
	{
		case 1:
		  return "Subcriber";
		break;
		case 2:
		  return "Contributor";
		break;
		case 3:
		  return "Editor";
		break;
		case 4:
		  return "Admin";
		break;
	} 

}