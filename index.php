<?php
  session_start();
  require_once"config.php";
  if(isset($_SESSION['dataError'])){
    $errorMessage="<span class='error'>Form not Properly Filled</span>";
	unset($_SESSION['dataError']);
  }
  if(isset($_SESSION['loginFailed'])){
    $errorMessage="<span class='error'>Login Failed</span>";
	unset($_SESSION['loginFailed']);
  }
  $title="";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="css/styles.css" rel="stylesheet" type="text/css" />
		<title>Online Library System</title>
	</head>
	<body>
		<h1>Online Library System</h1>
		<form method="POST" action="processor.php">
		<table>
			<tr>
				<td colspan="2">
					<?php if(isset($errorMessage)) echo $errorMessage; ?>
				</td>
			</tr>
			<tr>
				<td>Email</td>
				<td><input name="email" type="text" placeholder="Enter Email"/></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><input name="password" type="password" placeholder="Enter Password"/></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input id="submit" name="login" value="LOGIN" type="submit"/></td>
			</tr>
			<tr>
				<td><a href="">FORGOT PASSWORD</a></td>
				<td><a href="register">REGISTER</a></td>
			</tr>
		</table>
		</form>

	</body>
</html>
