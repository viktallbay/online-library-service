-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 13, 2013 at 03:02 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `olsweek`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` tinytext CHARACTER SET utf8 NOT NULL,
  `summary` text CHARACTER SET utf8 NOT NULL,
  `picture` varchar(256) CHARACTER SET utf8 NOT NULL,
  `ebook` varchar(256) CHARACTER SET utf8 NOT NULL,
  `category` int(10) unsigned NOT NULL,
  `uploadtime` datetime NOT NULL,
  `confirmtime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `summary`, `picture`, `ebook`, `category`, `uploadtime`, `confirmtime`) VALUES
(1, 'History of Colonial Nigeria ', 'Platea penatibus, placerat a cum et? Nec ut. Porttitor? Urna est a? Magnis. Vut? Aliquam dolor, arcu augue massa in? Non penatibus aliquam duis, nunc sociis porttitor duis rhoncus lectus ac pulvinar, tristique? Nunc scelerisque, pellentesque duis odio aliquam mauris odio in eu adipiscing et hac urna dignissim, vut egestas phasellus,', '1386943262127.0.0.11383122877127.0.0.1.png', '1386943262127.0.0.1data bundle.pdf', 5, '2013-12-13 15:01:02', '2013-12-13 15:18:19'),
(2, 'Mathematical Programming for Expert', 'Sociis montes tempor aenean ultrices nunc odio nisi nec, sagittis sagittis, tincidunt, pulvinar! Cursus tincidunt vel elementum placerat nec, sed velit pellentesque nunc. Ac, hac dignissim et nec in dapibus tempor a proin, magnis porta tincidunt sociis nec massa, proin dapibus placerat dictumst? In turpis aenean platea! Et. Nascetur', '1386943822127.0.0.11383123064127.0.0.1.png', '1386943822127.0.0.1SEO.docx', 1, '2013-12-13 15:10:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Mathematics'),
(2, 'Sciences'),
(3, 'French Languages'),
(4, 'Geography'),
(5, 'History'),
(6, 'Economics');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 NOT NULL,
  `email` varchar(128) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(16) CHARACTER SET utf8 NOT NULL,
  `password` varchar(256) CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(256) CHARACTER SET utf8 NOT NULL,
  `authority` tinyint(3) unsigned NOT NULL COMMENT '1 for subscriber, 2 for contributor, 3 for editor 4 for admin',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `phone`, `password`, `avatar`, `authority`) VALUES
(1, 'Adebayo Alabi', 'alabi10@yahoo.com', '08034265103', 'c95aed17a7d88c58e5b94dc1c6320a61', '1386934039127.0.0.1Avatar.jpg', 4),
(2, 'Victor O', 'viktallbay@gmail.com', '08075792992', 'ffc150a160d37e92012c196b6af4160d', '1386940008127.0.0.1wkclassbg.png', 1),
(3, 'Michael Abang', 'mcabang@yahoo.com', '08029799779', '0acf4539a14b3aa27deeb4cbdf6e989f', '1386940075127.0.0.1adeboye1.jpg', 3),
(4, 'Edafe Z', 'edafe.zinigbe@yahoo.com', '08064104494', '291a7a2c7283f77eed50601bd36a5bca', '1386940141127.0.0.1idahosa.jpg', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
