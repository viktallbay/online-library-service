<?php
  require_once '../dbconfig.php';
  require_once '../key.php';
  require_once '../functions.php';
  $title="Profile";
  
  //Generate Success Message
  if (isset($_SESSION['changeSuccessful'])) $message="User Right Changed";
  unset($_SESSION['changeSuccessful']);
  if (isset($_SESSION['uploadSuccessful'])) $message="Book uploaded awaiting approval";
  unset($_SESSION['uploadSuccessful']);
  if (isset($_SESSION['approvedSuccessful'])) $message="Book successfully approved";
  unset($_SESSION['approvedSuccessful']);
  
  //Get login user status
  $sql="SELECT authority
  FROM user
  WHERE
  id='".sanitizeMySQL($dbConnect, $_SESSION['loggedId'])."'";
  $result = mysqli_query($dbConnect, $sql);
  if (!$result)
  {
    $_SESSION['error']="User ID failed from submit: ". mysqli_error($dbConnect);
    header("Location: $url"."error/");
    exit();
  }
  $row = mysqli_fetch_array($result);
  $right=$row['authority'];
  
  //Generate HTML output base on user's authority's status
  switch ($right)
   {
      case 1:
	  //Subscriber
	  $output="<h2>WELCOME</h2> 
	  Check the latest <a href='$url"."library'>Book</a>";
      break;
	  
	  case 2:
	  //Contributor
	  //Fetch category from Db
	$sql="SELECT id, name
	  FROM category";
	  $result = mysqli_query($dbConnect, $sql);
	  if (!$result)
	  {
	  	$_SESSION['error']="Fetching category failed: ". mysqli_error($dbConnect);
        header("Location: $url"."error/");
		exit();
	  }
	//Construct HTML for display
	$tablePart1="<table border='1'>
	  <form method='post' action='processcontributor.php' enctype='multipart/form-data'>
	    <tr>
	      <td>Title of book</td>
	      <td><input type='text' name='title'></td>
	    </tr>
	    <tr>
	      <td>Summary of book </td>
	      <td><textarea name='content'></textarea></td>
	    </tr>
	    <tr>
	      <td>Upload File</td>
	      <td><input type='file' name='picture'></td>
	    </tr>
	    <tr>
	      <td>Category</td> 
	      <td> <select name='category'>";
	      $tablePart2="";
	   	  while ($row = mysqli_fetch_array($result))
	   	  {
	   	    $tablePart2=$tablePart2."
	   	    <option value='".$row['id']."'>".$row['name']."</option>";
		  }
	      $tablePart3="</select></td></tr>
	    <tr>
	    <input type='hidden' name='MAX_FILE_SIZE' value='8512000'/>
	      <td>Upload SoftCopy<br/>
	      (max 8.5MB)</td>      
	      <td><input type='file' name='upload'></td>
	    </tr>
	    <tr>
	      <td colspan='2'><input type='submit' name='contributor' value='Submit Material'></td>
	    </tr>
	  </form>
	</table>";
	$output=$tablePart1.$tablePart2.$tablePart3;
          
      break;
	  
	  case 3:
	  //Editor
      $sql="SELECT book.id, book.title, book.summary, category.name, book.picture, book.ebook
	  FROM book INNER JOIN category
	  ON book.category = category.id
	  WHERE confirmtime='0000-00-00 00:00:00'";
	  $result = mysqli_query($dbConnect, $sql);
	  if (!$result)
	  {
	  	$_SESSION['error']="Material selection failed: ". mysqli_error($dbConnect);
        header("Location: $domainName"."error/");
		exit();
	  }
	  
	  //Construct HTML for display
	  $tableHead="<table border='1' width='700'>
	    <tr>
	      <td>Title</td> <td>Summary</td> <td>Picture</td> 
	      <td>Category</td> <td>Download</td>  <td></td> 
	    </tr>";
	  $tableMiddle="";
	   while ($row = mysqli_fetch_array($result))
	   {
	   	$tableMiddle=$tableMiddle."<tr>
	   	<td>".$row['title']."</td>
	   	<td>".$row['summary']."</td>
	   	<td>".$row['name']."</td>
	   	<td><img src='$url"."bookpictures/".$row['picture']."'/></td>
	   	<td><a href='$url"."download.php?f=".$row['ebook']."'>Download</a></td>
		<td><form action='processeditor.php' method='post'>
		   <input type='hidden'name='id' value='".$row['id']."'>
		  <input type='submit' name='editor' value='Approve'></form>
		</td></tr>";
	   }
	   $tableTail="</table> ";
	   $output=$tableHead.$tableMiddle.$tableTail;
      break;
	  
	  case 4:
	  //Admin
      $sql="SELECT id, name, authority
	  FROM user";
	  $result = mysqli_query($dbConnect, $sql);
	  if (!$result)
	  {
	  	$_SESSION['error']="All user selection from submit: ". mysqli_error($dbConnect);
        header("Location: $domainName"."error/");
		exit();
	  }
	  
	  //Construct HTML for display
	  $tableHead="<table border='1'>
	    <tr>
	      <td>Name</td> <td>Present Right</td> <td>New Right</td>
	    </tr>";
	  $tableMiddle="";
	   while ($row = mysqli_fetch_array($result))
	   {
	   	$tableMiddle=$tableMiddle."<tr>
	   	<td>". $row['name'] ." </td> 
	   	<td>".getRight($row['authority'])."</td>
		<td><form action='processadmin.php' method='post'>
		  <select name='newright'>
		    <option></option>
		    <option value='1'>Subscriber</option>
		    <option value='2'>Contributor</option>
		    <option value='3'>Editor</option>
		    <option value='4'>Admin</option>
		  </select>
		   <input type='hidden'name='id' value='".$row['id']."'>
		  <input type='submit' name='admin' value='Change'></form>
		</td></tr>";
	   }
	   $tableTail="</table> ";
	   $output=$tableHead.$tableMiddle.$tableTail;
      break;
      
  }
  
  require_once '../templates/header.php';