<?php
  require_once 'controller.php';
  $pageName="<span id='pageName'>Profile</span>";
?>
	<body>
		<?php require_once '../templates/whitebarleft.php'; ?>
			<div class="left" id="wrapperRight">
				<?php if(isset($message)) echo $message; ?>
				<?php echo $output; ?>
			</div><!--right wrapper-->
		</div><!--wrapper-->
		<?php require_once '../templates/footer.php'; ?>
	</body>
</html>
