<?php
  require_once '../config.php';
  require_once '../dbconfig.php';
  require_once '../functions.php';
  if ((isset($_POST['contributor'])))
  {
	//Extract data sent	
  	$title=sanitizeMySQL($dbConnect, $_POST['title']);
	$content=sanitizeMySQL($dbConnect, $_POST['content']);
	$category=sanitizeMySQL($dbConnect, $_POST['category']);
	
	//Extract uploaded book softcopy's file
	$file1 = time() . $_SERVER['REMOTE_ADDR'] . basename($_FILES['upload']['name']);
	$fileLocation = "$systemRoot"."booksoftcopy/".$file1; 
	//move file
	if (!is_uploaded_file($_FILES['upload']['tmp_name']) || !copy($_FILES['upload']['tmp_name'], $fileLocation))
	  {
		$_SESSION['error']="Moving file failed: on line".__LINE__;
        header("Location: $url"."error/");
		exit();
	  }
	  
	 //Extract uploaded book softcopy's file
	$filepic = time() . $_SERVER['REMOTE_ADDR'] . basename($_FILES['picture']['name']);
	$filepicLocation = "$systemRoot"."bookpictures/".$filepic; 
	//move file
	if (!is_uploaded_file($_FILES['picture']['tmp_name']) || !copy($_FILES['picture']['tmp_name'], $filepicLocation))
	  {
		$_SESSION['error']="Moving file failed: on line ".__LINE__;
        header("Location: $url"."error/");
		exit();
	  }
	
	//Store data in Db
	
	$sql="INSERT INTO book
	  (ebook, category, title, summary, uploadtime, picture)
	  VALUES
	  ('$file1', '$category', '$title', '$content', NOW(), '$filepic')";
	
	if (!mysqli_query($dbConnect, $sql))
	{
	 $_SESSION['error']="Changing user right failed".mysqli_error($dbConnect);
     header("Location: $url"."error/");
     exit();
    }
	
	//Get editor's email and phone nos
	$sql="SELECT email, phone, name
	  FROM user
	  WHERE
	  authority='3'";
	$result = mysqli_query($dbConnect, $sql);
	if (!$result)
	{
	  $_SESSION['error']="Error User login failed".mysqli_error($dbConnect)." in ".__FILE__."  on ".__LINE__." ";
      header("Location: $domainName"."error/");
      exit();
    }
	$row = mysqli_fetch_array($result);
	foreach ($row as $anEditor) {
	  $phone='234'.ltrim($anEditor['phone'],'0');
	  $username = 'disputeBabcock'; //your username 
	  $password = 'disputeBabcock'; //your password 
	  $sender = 'OLS'; //sender ID
	  $message = "Good Day Mr Editor a book has been added to the online library system and it is awaiting your approval";
	  // Infobip's POST URL
	  $postUrl = "http://api2.infobip.com/api/sendsms/xml";
	  
	  // XML-formatted data IMMEDIATE message
	  $xmlString =
	    "<SMS>
		  <authentification>
		    <username>$username</username>
			<password>$password</password>
		  </authentification>
		  <message>
		    <sender>$sender</sender>
			<text>$message</text>
		  </message>
		  <recipients>
		    <gsm>$phone</gsm>
		  </recipients>
		</SMS>";
		// previously formatted XML data becomes value of "XML" POST variable
		$fields = "XML=" . urlencode($xmlString);
		// in this example, POST request was made using PHP's CURL
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $postUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		//response of the POST request
		$response = curl_exec($ch);
		curl_close($ch);
		// write out the response
		
		//Send email to the editor
		$time=date("l F jS, Y - g:ia", time());
		$to=$anEditor['email'];
  		$subject="OLS Book Awaiting Approval";
  		$headers  = 'MIME-Version: 1.0' . "\r\n";
  		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
  		$headers .= "From: Online Library System <noreply@gradedinfo.com>" . "\r\n";
  		$message="
  		<html>
		   <head>
		     <title>$subject</title>
		   </head>
		   <body>
		     <div style='width:75%; padding:40px 10% 15px 10%; background-color: #f1f1f1;'>
		     <p style='margin-bottom:10px; clear: both; font-size: 14px; font-weight: bold;'>Good day {$anEditor['name']}</p>
			 <p style='margin-bottom:10px;'>
			   The is a new book that has been added to Online Library system since $time and it is awaiting your approval  
			 </p>
			 <p style='margin-bottom:10px; margin-left:5%; height=160px;'>
			 	Book Title<br/>
			 	$title
			</p>
			<p style='margin-bottom:10px; margin-left:5%; height=160px;'>
			 	Book Summary<br/>
			 	$content
			</p>
			<br/><br/><br/>
			<p style='font-size:9px;'>
			     Powered by <a style='color:#000;' href='http://www.alabiansolutions.com'>Alabian Solutions</a>
			</p>
			 </div>
		   </body>
		 </html>";
  		mail($to, $subject, $message, $headers);	
	  }

	  	$_SESSION['uploadSuccessful']=TRUE;
	header("Location: .");
  }
  else {
    $_SESSION['error']="Fail attack from registration form ";
	header("Location: $url"."error/");
  }


  